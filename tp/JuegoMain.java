package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.InterfaceJuego;

public class JuegoMain extends InterfaceJuego {
	private static Fondo fondo;
	private Princesa princesa;
	
	// El objeto Entorno que controla el tiempo y otros
	private Entorno entorno;
	
	public JuegoMain() {
		// Inicializa el objeto entorno
		this.entorno = new Entorno(this, " Super Elizabeth Sis, Volcano Edition ", 800, 600);
		JuegoMain.fondo = new Fondo("fondo.png",400,200);
		this.princesa = new Princesa(400, 500, 2.0); 
		
		// Inicia el juego!
		this.entorno.iniciar();
	}

	/**
	 * Durante el juego, el método tick() será ejecutado en cada instante y 
	 * por lo tanto es el método más importante de esta clase. Aquí se debe 
	 * actualizar el estado interno del juego para simular el paso del tiempo 
	 * (ver el enunciado del TP para mayor detalle).
	 */
	public void tick() {
		fondo.dibujar(entorno);
		
		if (entorno.estaPresionada(entorno.TECLA_IZQUIERDA)) {
			princesa.moverHaciaIzquierda(true);
		}else {
			princesa.moverHaciaIzquierda(false);
		}
		if (entorno.sePresiono(entorno.TECLA_DERECHA)) {
			princesa.moverHaciaDerecha();
		}
		dibujarPrincesa();
		
		}

    private void dibujarPrincesa() {
        if (princesa.getDireccion().equals("izquierda")) {
            entorno.dibujarImagen(princesa.getImagenIzquierda(), princesa.getX(), princesa.getY(), princesa.getAngulo());
        } else {
            entorno.dibujarImagen(princesa.getImagenDerecha(), princesa.getX(), princesa.getY(), princesa.getAngulo());
        }
    }
		

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		JuegoMain juego = new JuegoMain();
	}

}
