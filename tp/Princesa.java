//Modelo ver archivo Slug.java y Nave.java
package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Princesa {
	private double x;
	private double y;
	private double velocidad;
	private double angulo;
	private String direccion;
	private Image prinderecha;
	private Image prinizquierda;
	private boolean teclaIzquierdaPresionada;
	
	

	public Princesa(int x, int y, double velocidad) {
	    this.x = x;
	    this.y = y;
	    this.velocidad = velocidad; 
	    this.angulo = 0;
	    this.prinderecha = Herramientas.cargarImagen("marioderecha.png");
	    this.prinizquierda = Herramientas.cargarImagen("marioizquierda.png");
	    this.direccion = "";
	    this.teclaIzquierdaPresionada = false;
	}
	
	public void moverHaciaDerecha() {
	    x += velocidad; 
	    direccion = "derecha"; 
	}

	public void moverHaciaIzquierda(boolean presionada) {
		teclaIzquierdaPresionada = presionada;
		if (presionada) {
			x -= velocidad*2;
			direccion = "izquierda";
		}
	}
	public void teclaIzquierdaPresionada(boolean presionada) {
		this.teclaIzquierdaPresionada = presionada;
	}
	public void dibujar(Entorno entorno) {
	    if (teclaIzquierdaPresionada) {
	        entorno.dibujarImagen(prinizquierda, x, y, angulo);
	    } else {
	        entorno.dibujarImagen(prinderecha, x, y, angulo);
	    }
	}



	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getVelocidad() {
		return velocidad;
	}

	public double getAngulo() {
		return angulo;
	}
	public Image getImagenDerecha() {
		return prinderecha;
	}
	public Image getImagenIzquierda() {
		return prinizquierda;
	}
	public String getDireccion() {
		return direccion;
	}
}

